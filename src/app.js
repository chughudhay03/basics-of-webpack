// let sample = () => console.log("Hello World");

// sample();

//Common JS Module
// const Library = require('./lib');
// console.log(Library.sayHello("Study Link"));
// console.log(Library.student);

const OldModule = require('./oldWayOfExport');
console.log(OldModule);

//ES2015 Method to import
// import {student, sayHello} from './lib.js';
// sayHello(student.name);

// Alternate way to import the above code
import * as Library from './lib';
console.log(Library.sayHello("Study"));
console.log(Library.student);