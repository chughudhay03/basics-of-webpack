// Module Exports
module.exports = {
    name: 'John Doe',
    age: function() {
        console.log("Hello " + this.name);
    },
    college: 'TSEC'
};