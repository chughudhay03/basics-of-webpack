export const student = {
    name: 'John Doe',
    age: 25,
    college: 'I Dont Care'
};

export function sayHello(name) {
    console.log("Hello: " + name);
};

export const teacher = {
    name: 'IDK',
    location: 'Mumbai'
};